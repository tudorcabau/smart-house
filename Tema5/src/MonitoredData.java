import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MonitoredData implements Serializable {
	private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
	private Date dateStart, dateEnd ;
	private String activitate;
	public MonitoredData() {
		
	}
@Override
public String toString(){
	return dateTimeFormat.format(dateStart) + "		" + dateTimeFormat.format(dateEnd) + "		" + this.activitate + "\n";
}
public Date getDateStart() {
	return dateStart;
}
public void setDateStart(String stringDateStart) throws ParseException {
	
	this.dateStart = dateTimeFormat.parse(stringDateStart);
}
public Date getDateEnd() {
	return dateEnd;
}
public void setDateEnd(String stringDateEnd) throws ParseException {
	this.dateEnd =  dateTimeFormat.parse(stringDateEnd);
}
public String getActivitate() {
	return activitate;
}
public void setActivitate(String activitate) {
	this.activitate = activitate;
}

}
