import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.security.auth.login.AppConfigurationEntry;

import com.google.common.base.Splitter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception{
	Parent root = FXMLLoader.load(getClass().getResource("/View/View.fxml"));
	primaryStage.setTitle("Aplicatie");
	primaryStage.setScene(new Scene(root));
	primaryStage.show();
	}
	
	public static void main(String[] args) throws IOException{

		Functions instantaFunctii = Functions.getInstance();
		instantaFunctii.countDistinctDays();
		instantaFunctii.countActivityOccurence();
		instantaFunctii.countActivityOccurenceDaily();
		instantaFunctii.countActivityTimespent();
		instantaFunctii.countActivityTimeSpentAverage();
		
		launch(args);
	
}
}
