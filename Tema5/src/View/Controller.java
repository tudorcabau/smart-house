package View;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Hashtable;

import com.google.common.base.Functions;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

public class Controller  {
	@FXML
    private TextArea textArea;
	   @FXML
	    void cerinta1(ActionEvent event) throws FileNotFoundException, IOException, ClassNotFoundException {
	    	ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("NumberOfActivities.bin"));
			String readObject =  objectInputStream.readObject().toString();
			System.out.println(readObject);
			textArea.setText(readObject);
			objectInputStream.close();
	    }
	    @FXML
	    void cerinta2(ActionEvent event) throws ClassNotFoundException, IOException {
	    	ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("ActivitiesOccurence.bin"));
			String readObject =  objectInputStream.readObject().toString();
			System.out.println(readObject);
			textArea.setText(readObject);
			objectInputStream.close();
	    }

	    @FXML
	    void cerinta3(ActionEvent event)throws ClassNotFoundException, IOException {
	    	ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("ActivitiesPerDay.bin"));
			String readObject =  objectInputStream.readObject().toString();
			System.out.println(readObject);
			textArea.setText(readObject);
			objectInputStream.close();
	    }

	    @FXML
	    void cerinta4(ActionEvent event)throws ClassNotFoundException, IOException {
	    	ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("ActivitiesTimeSpent.bin"));
			String readObject =  objectInputStream.readObject().toString();
			System.out.println(readObject);
			textArea.setText(readObject);
			objectInputStream.close();
	    }

	    @FXML
	    void cerinta5(ActionEvent event)throws ClassNotFoundException, IOException {
	    	ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("ActivitiesTimeSpentAverage.bin"));
			String readObject =  objectInputStream.readObject().toString();
			System.out.println(readObject);
			textArea.setText(readObject);
			objectInputStream.close();
	    }
}
