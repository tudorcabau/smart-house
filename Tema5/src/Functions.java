import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Collectors;
import com.google.common.base.Splitter;

public class Functions {
	private static Function<String, MonitoredData> mapLineToMonitoredData;
	private static List<MonitoredData> monitoredData = null;
	private static Functions instance = null;

	protected Functions() {
		mapLineToMonitoredData = new Function<String, MonitoredData>() {
			public MonitoredData apply(String line) {
				MonitoredData monitoredEntry = new MonitoredData();
				List<String> monitoredDataPieces = Splitter.on("		").trimResults().omitEmptyStrings()
						.splitToList(line);
				try {
					monitoredEntry.setDateStart(monitoredDataPieces.get(0));
					monitoredEntry.setDateEnd(monitoredDataPieces.get(1));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				monitoredEntry.setActivitate(monitoredDataPieces.get(2));
				return monitoredEntry;
			}
		};
		try {
			monitoredData = Files.lines(Paths.get("C:/Users/cabau/workspace/Tema5/src/Activity/Activities.txt"))
					.map(mapLineToMonitoredData).collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println(e);
		}
		for (MonitoredData data : monitoredData) {
			System.out.println(data.toString());
		}
	}

	public static Functions getInstance() {
		if (instance == null) {
			instance = new Functions();
		}
		return instance;
	}

	public List<MonitoredData> getMonitoredData() {
		return monitoredData;
	}
	public int countDistinctDays(){
		int distinctDates = monitoredData
	    .stream()
	    .collect(Collectors.groupingBy(p -> p.getDateStart().toString().substring(0, 10))).size();
		
		System.out.println(distinctDates + " zile distincte");
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(new FileOutputStream("NumberOfActivities.bin"));
			objectOutputStream.writeObject(distinctDates);
		} catch (IOException e) {
			System.out.println(e);
		}
		
		return distinctDates;
	}
	public int countActivityOccurence(){

		
		Map<String, Long> activityOccurence =  monitoredData
			    .stream()
			    .collect(Collectors.groupingBy(p->p.getActivitate(),Collectors.counting()));
			    
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(new FileOutputStream("ActivitiesOccurence.bin"));
			objectOutputStream.writeObject(activityOccurence);
		} catch (IOException e) {
			System.out.println(e);
		}
		
		for(String key : activityOccurence.keySet()){
			System.out.println(key + " " + activityOccurence.get(key).intValue());
		}
		
		return 0;
	}
		public int countActivityOccurenceDaily(){
			Map<String, Map<String, Long>> distinctDates = monitoredData
				    .stream()
				    .collect(Collectors.groupingBy(	p -> p.getDateStart().toString().substring(0, 10),(Collectors.groupingBy(p->p.getActivitate(),Collectors.counting()))));
			StringBuilder sb = new StringBuilder();
			for(String key : distinctDates.keySet()){
				System.out.println(key + " " + distinctDates.get(key));
				sb.append(key + " " + distinctDates.get(key) + "\n");
			}
			
			
			ObjectOutputStream objectOutputStream;
			try {
				objectOutputStream = new ObjectOutputStream(new FileOutputStream("ActivitiesPerDay.bin"));
				objectOutputStream.writeObject(sb);
			} catch (IOException e) {
				System.out.println(e);
			}		
	
			
			
			return 0;
		}
	public int countActivityTimespent(){

		
		Map<String, LongSummaryStatistics> activityOccurence =  monitoredData
			    .stream()
			    .collect(Collectors.groupingBy(p->p.getActivitate(),Collectors.summarizingLong(p->p.getDateEnd().getTime()-p.getDateStart().getTime())));
		Map<String , Long> activiyStatistics = activityOccurence.entrySet()
				.stream()
				.filter(s->s.getValue().getSum()>36000000)
				.collect(Collectors.toMap(s->s.getKey().toString(), s->s.getValue().getSum()));

		StringBuilder sb = new StringBuilder();
		for(String key : activiyStatistics.keySet()){
			System.out.println(key + " " + activiyStatistics.get(key).longValue()/3600000 + " hours");
			sb.append(key+ " " + activiyStatistics.get(key).longValue()/3600000 + " hours" + "\n");
		}
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(new FileOutputStream("ActivitiesTimeSpent.bin"));
			objectOutputStream.writeObject(sb);
		} catch (IOException e) {
			System.out.println(e);
		}		

		
		return 0;
	}
	int countAll = 0; 
	int countLess = 0 ;
	public int countActivityTimeSpentAverage(){

		
		Function<MonitoredData, Long> time = (MonitoredData m )->{return m.getDateEnd().getTime()-m.getDateStart().getTime();};
		List<String> timpPerActivitate = monitoredData
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivitate,Collectors.mapping(time, Collectors.toList())))
				.entrySet().stream().filter(e->e.getValue().stream().filter(s->s.longValue()<300000).count()>0.9*e.getValue().size())
				.map(e->e.getKey())
				.collect(Collectors.toList())
				;

			    
		System.out.println(timpPerActivitate);
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(new FileOutputStream("ActivitiesTimeSpentAverage.bin"));
			objectOutputStream.writeObject(timpPerActivitate);
		} catch (IOException e) {
			System.out.println(e);
		}		

		
		return 0;
		
	}
	
}
